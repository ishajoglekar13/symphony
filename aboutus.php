<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>About us</title>
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
        <link rel="stylesheet" href="assets/css/demo.css" />
        <link rel="stylesheet" href="assets/css/pater.css">
        <link rel="stylesheet" type="text/css" href="assets/css/demo.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/eventeffects.css" />
        <!-- <link rel="shortcut icon" href="../favicon.ico">  -->
        <link rel="stylesheet" type="text/css" href="assets/css/component.css" />
        <link rel="stylesheet" href="assets/css/sitemap.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.css" />
        <link rel="stylesheet" href="assets/css/owl.theme.default.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/about-us.css">
        <link rel="stylesheet" type="text/css" href="assets/css/index.css">
        <link rel="stylesheet" href="assets/css/timeline.css">
</head>
<body>
    
 <div class="content">
  <div id="main_bg">  
    <div class="main">
    <h1>About Symphony</h1>
    <p>
        Symphony is the annual cultural festival of K. J. Somaiya College of Engineering. Symphony started out in 1987 as Jamboree and since then has grown exponentially in all dimensions. Symphony has evolved as a brand that is a potent concoction of ceaseless imagination and endless possibilities. Be it exciting contests pertaining to art, dance, music, cooking, and much more, or star-studded pro-nights, Symphony has something for everyone. Personalities like Ustad Zakir Khan, Sushant Singh Rajput, The Local Train, Kenny Sebastian, Aditi Singh Sharma, and many more have graced Symphony with their presence. 
    </p>
    <!-- <hr> -->
    <br>
    <h1>About KJSCE</h1>
    <p>
    Set in the heart of Mumbai, KJSCE is located in the Somaiya Vidyavihar campus. With a total of 34 institutions in its 65 acre campus, Somaiya Vidyavihar has become an educational hub. Since its establishment in 1983, KJSCE has grown in leaps and bounds. Boasting of a huge campus and state of the art facilities KJSCE has surpassed many of its contemporaries and presented itself as one of the premier engineering colleges in Mumbai. And with another feather in its cap in the form of being the only self financed Engineering College to be granted autonomous status whilst maintaining its affiliation to the University of Mumbai.</p>
    <!-- <hr> -->
    <br>
    </div>
<section id=timeline>
  <!-- <h1>A Flexbox Timeline</h1> -->
  <!-- <p class="leader">All cards must be the same height and width for space calculations on large screens.</p> -->
  <div class="demo-card-wrapper">
    <div class="demo-card demo-card--step1">
      <div class="head">
<!--         <div class="number-box">
          <span>01</span>
        </div> -->
        <p><span class="small">1987</span><br>Maiden edition of Symphony under the name of “Jamboree” featuring events like Treasure Hunt and Musical Antakshari.</p>
      </div>
      <div class="body">

        <img src="assets/images/timeline/1987a.png" alt="Graphic">
      </div>
    </div>

    <div class="demo-card demo-card--step2">
      <div class="head">
<!--         <div class="number-box">
          <span>02</span>
        </div> -->
        <p><span class="small">1994</span><br>Ustad Zakir Hussain performs at Symphony.</p>
      </div>
      <div class="body">

        <img src="assets/images/timeline/1994a.png" alt="Graphic">
      </div>
    </div>

    <div class="demo-card demo-card--step3">
      <div class="head">
<!--         <div class="number-box">
          <span>03</span>
        </div> -->
        <p><span class="small">1999</span><br>Symphony acclaimed as the ‘Best Festival of Mumbai’, featuring performances by notable stars like Hariharan, Devaki Pandit, Fazal Qureshi, Taufiq Qureshi, Louis Banks, and Sultan Khan.</p>
      </div>
      <div class="body">

        <img src="assets/images/timeline/1999a.png" alt="Graphic">
      </div>
    </div>

    <div class="demo-card demo-card--step4">
      <div class="head">
<!--         <div class="number-box">
          <span>04</span>
        </div> -->
        <p><span class="small">2009</span><br>Symphony celebrated for 3 days for the first time, on the occasion of the Silver Jubilee of K.J. Somaiya College of Engineering.</p>
      </div>
      <div class="body">

        <img src="assets/images/timeline/2009a.png" alt="Graphic">
      </div>
    </div>

    <div class="demo-card demo-card--step5">
      <div class="head">
<!--         <div class="number-box">
          <span>05</span>
        </div> -->
        <p><span class="small">2015</span><br>Symphony 2015 was graced by Sushant Singh Rajput and Anu Malik, and saw inception of Parvaah, the social initiative of K.J. Somaiya College of Engineering.</p>
      </div>
      <div class="body">

        <img src="assets/images/timeline/2015a.png" alt="Graphic">
      </div>
    </div>

    <div class="demo-card demo-card--step6">
      <div class="head">
<!--         <div class="number-box">
          <span>06</span>
        </div> -->
        <p><span class="small">2017</span><br>Symphony hits record-breaking footfall of 15,000, featuring performances from renowned international as well as homegrown artists such as DJ Candice, Kenny Sebastian, and Lagori.</p>
      </div>
      <div class="body">

        <img src="assets/images/timeline/2017a.png" alt="Graphic">
      </div>
    </div>
    <div class="demo-card demo-card--step6">
      <div class="head">
<!--         <div class="number-box">
          <span>06</span>
        </div> -->
        <p><span class="small">2017</span><br>Symphony hits record-breaking footfall of 15,000, featuring performances from renowned international as well as homegrown artists such as DJ Candice, Kenny Sebastian, and Lagori.</p>
      </div>
      <div class="body">

        <img src="assets/images/timeline/2017a.png" alt="Graphic">
      </div>
    </div>
    
    
    
  </div>
</section>
</div>
</div>   
    
  </div>
</section>
</body>
</html>