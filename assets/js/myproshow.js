$(document).ready(function(){
                  $("#proshowcar").owlCarousel({
                      items:1,
                      autoplay:true,
                      margin:20,
                      loop:true,
                      nav:true,
                      smartSpeed:1000,
                      autoplayHoverPause:true,
                      dots:false,
                      navText:['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>']
                  });
    });