<!DOCTYPE html>
<html>
  <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta
    name="viewport"
    content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes"
    />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>Reach Us</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="assets/css/demo.css" />
    <link rel="stylesheet" href="assets/css/pater.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.css" />
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.blue.css">
    <link rel="stylesheet" href="assets/css/reachus.css" />
    <link rel="stylesheet" href="assets/css/sitemap.css" />
    <link rel="stylesheet" href="assets/css/index.css">
      <!-- Latest compiled and minified CSS & JS -->
      <link rel="stylesheet" media="screen"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

      <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-132742730-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-132742730-1');
    </script>
  </head>
  <body class="demo-6" id="reach-us-page">
    <main class="main--demo-6">
      <div class="content--demo-6">
        <!-- HAMBURGER ICON -->
        <div class="hamburger hamburger--demo-6 js-hover">
          <div class="hamburger__line hamburger__line--01">
            <div
              class="hamburger__line-in hamburger__line-in--01 hamburger__line-in--demo-5"
            ></div>
          </div>
          <div class="hamburger__line hamburger__line--02">
            <div
              class="hamburger__line-in hamburger__line-in--02 hamburger__line-in--demo-5"
            ></div>
          </div>
          <div class="hamburger__line hamburger__line--03">
            <div
              class="hamburger__line-in hamburger__line-in--03 hamburger__line-in--demo-5"
            ></div>
          </div>
          <div class="hamburger__line hamburger__line--cross01">
            <div
              class="hamburger__line-in hamburger__line-in--cross01 hamburger__line-in--demo-5"
            ></div>
          </div>
          <div class="hamburger__line hamburger__line--cross02">
            <div
              class="hamburger__line-in hamburger__line-in--cross02 hamburger__line-in--demo-5"
            ></div>
          </div>
        </div>
        <!-- TOP LEFT IMAGE GOES HERE, height 100px -> 50px -->
        <div class="header">
          <a href=" index.php"><img id="logo" src="assets/images/2019/Updated symphony logo withtheme.svg" alt=""/></a>
        </div>
        <!-- MAIN CONTENT GOES HERE, height calc(100vh - 200px) -> calc(100vh - 100px) -->
        <div class="image_container" id="address">
        
              <img src="./assets/images/address.png" alt="">
            
          </div>
        
        <div class="container" id="reachus">
            <div class="reachus-logos">
                <img src="./assets/images/2020/truss%20logo.png" alt="">
            <img src="./assets/images/2020/truss%20logo.png" alt="">
            </div>
            <h1 class="reachus-title text-center">Reach Us</h1>
        </div>
         <footer class="footer">
<!--
        <div>
            <p>Developed by <a href="https://github.com/Gaurav-Punjabi">Gaurav Punjabi</a> &amp; <a href="https://github.com/yukta12">Yukta Peswani</a></p>
        </div>
-->
    </footer>
        
        <!-- SITEMAP -->
        <?php
        require_once("./includes/sitemap.php");
        ?>
      </div>
    </main>
    
    <script src="assets/js/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="assets/js/easings.js"></script>
    <script src="assets/js/demo.js"></script>
    <script src="assets/js/demo6.js"></script>
    <script src="assets/js/owl.carousel.js"></script>
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    
    <script>
    $(document).ready(function() {
        $(".team-members").owlCarousel({
            items: 3,
            autoplay: true,
            smartSpeed: 700,
            // loop: true,
            dots:true,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    autoplay: true,
                    items: 1,
                    startPosition:2,
                    dots:true,
                    // loop:true,
                    autoplay:true,
                    autoplayTimeout:5000,
                    autoplayHoverPause: false,
                    // smartSpeed: 100,
                },
                480: {
                    items: 2,
                },
                768: {
                    items: 5,
                }
            }
        });
    });
    </script>
  </body>
</html> 